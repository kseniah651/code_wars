def rgb(r, g, b):
    r = check_len(two_to_sixteen(bin(check(r))[2:]))
    g = check_len(two_to_sixteen(bin(check(g))[2:]))
    b = check_len(two_to_sixteen(bin(check(b))[2:]))
    return r + g + b

def two_to_sixteen(x):
    d = {"0000": "0", "0001": "1", "0010": "2", "0011": "3", "0100": "4",
    "0101": "5", "0110": "6", "0111": "7", "1000": "8", "1001": "9", "1010": "A",
    "1011": "B", "1100": "C", "1101": "D", "1110": "E", "1111": "F"}
    s = ""
    if len(x) % 4 !=0:
        x = '0'*((len(x)//4 + 1)*4 - len(x)) + x
    for i in range(0,len(x),4):
        s += d[x[i:i+4]]
    return s

def check(x):
    if x < 0:
        x = 0
    if x > 255:
        x = 255
    return x

def check_len(x):
    if len(x) < 2:
        x = '0'*((len(x)//2 + 1)*2 - len(x)) + x
    return x
