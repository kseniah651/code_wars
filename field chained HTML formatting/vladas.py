class Format:
    tags = frozenset(("div", "h1", "p", "span"))
    this_ancestors = []

    def __init__(self, new_ancestors = []):
        self.this_ancestors = new_ancestors

    def __getattr__(self, name):
        if(name in self.tags):
            return Format(self.this_ancestors + [name])
        else:
            return super(Format, self).__getattribute__(name)

    def __call__(self, *content):
        open_tags = ''.join([f'<{tag}>' for tag in self.this_ancestors])
        content_tag = ''.join(content)
        close_tags = ''.join([f'</{tag}>' for tag in self.this_ancestors[::-1]])
        return f'{open_tags}{content_tag}{close_tags}'

format = Format()
print(format.p.span.h1('cq'))